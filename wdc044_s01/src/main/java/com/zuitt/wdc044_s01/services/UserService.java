package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.User;

public interface UserService {
    void createUser(User user);
    void updateUser(Long id, User user);
    void deleteUser(Long id, User user);
    Iterable<User> getUsers();
}
