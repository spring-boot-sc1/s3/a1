package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        userRepository.save(user);
    }

    public void updateUser(Long id, User user){
        User userForUpdating = userRepository.findById(id).get();
        userForUpdating.setUserName(user.getUserName());
        userForUpdating.setPassword(user.getPassword());
        userRepository.save(userForUpdating);
    }

    public void deleteUser(Long id, User user){
        userRepository.deleteById(id);
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

}
