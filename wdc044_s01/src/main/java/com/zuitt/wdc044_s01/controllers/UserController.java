package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.User;;
import com.zuitt.wdc044_s01.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin

public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value="/user", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

}
