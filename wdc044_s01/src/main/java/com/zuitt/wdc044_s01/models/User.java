package com.zuitt.wdc044_s01.models;

import javax.persistence.*;


@Entity

@Table(name="users")
public class User {

    @Id
    @GeneratedValue
    private long id;

    @Column
    private String userName;

    @Column
    private String password;


    public User(){}

    public User(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    public String getUserName(){
        return userName;
    }

    public void setUserName(String userName){
        this.userName= userName;
    }

    public String getPassword(){
        return  password;
    }

    public void setPassword(String password){
        this.password = password;
    }
}
