package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

@Entity // mark this java object as database table

//name the table
@Table(name="posts")
public class Post {
    //indicate the primary key
    @Id

    //auto-increment the ID column
    @GeneratedValue
    private long id;

    @Column
    private String title;

    @Column
    private String content;

    //default constructor needed when retrieving posts
    public Post(){}

    //other necessary constructors
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return  content;
    }

    public void setContent(String content){
        this.content = content;
    }
}
